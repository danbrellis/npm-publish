# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.2

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.3.1

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.0

- minor: Support for private npm registry.

## 0.2.8

- patch: Internal maintenance: Add gitignore secrets.

## 0.2.7

- patch: Update the Readme with a new Atlassian Community link.

## 0.2.6

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.5

- patch: Documentation updates

## 0.2.4

- patch: Refactor pipe code to use pipes bash toolkit.

## 0.2.3

- patch: Updated contributing guidelines

## 0.2.2

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines npm deployment pipe.

